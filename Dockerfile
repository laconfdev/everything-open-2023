FROM jekyll/builder:4.2.0 as build
COPY . /srv/jekyll
RUN set -x \
    && rm -f Gemfile* \
    && mkdir -p /dest \
    && chown -R jekyll:jekyll /dest \
    && jekyll build --destination /dest

FROM nginx:alpine
COPY --from=build /dest /usr/share/nginx/html
COPY nginx.conf /etc/nginx
