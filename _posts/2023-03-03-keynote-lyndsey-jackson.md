---
layout: post
title: "Keynote: Lyndsey Jackson"
card: keynote-lyndsey.63663717.png
---

<p class="lead">Everything Open 2023 is excited to announce our second keynote speaker: Lyndsey Jackson.</p>

<img class="img-fluid float-sm-end profile-pic" src="/media/img/lyndsey.png" width="400">


## About Lyndsey Jackson

Lyndsey is the CEO and Co-founder of AgTech business [Platfarm](https://www.platfarm.com/). 
Lyndsey has been working with Open Source technologies for over 15 years, with involvement spanning many communities such as Drupal, GovHack, Electronic Frontiers Australia (EFA) and Open Agriculture. 
Living in regional South Australia, Lyndsey is passionate about bringing technology experiences to those living outside the major population centres in Australia, connecting them with the Open technology community and inspiring them to use technology to express themselves and realise their aspirations. 

A committed advocate for inclusion, diversity, and digital skill development, she believes that broadening participation in tech has the power to enrich and transform regional lives, communities, economies, and main streets.

We look forward to hearing from Lyndsey at Everything Open 2023.


## Attend Everything Open

With the conference rapidly approaching, now is the time to purchase your ticket to attend Everything Open 2023!
Make sure you [register today](/attend/tickets/) to hear from Lyndsey and the rest of our great lineup.

Make sure to check out our range of merchandise available for purchase via our [Redbubble shop](https://www.redbubble.com/shop/ap/140087437), especially while there is a 20% off sale.
We have a number of different items available for purchase so you can get a memento of attending Everything Open 2023.

If you have questions please contact the Organising Team via email at [contact@everythingopen.au](mailto:contact@everythingopen.au).
