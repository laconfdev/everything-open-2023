---
layout: post
title: "Keynote: Seb Chan"
card: keynote-seb.3768974e.png
---

<p class="lead">We are excited to announce Seb Chan as a keynote presenter for Everything Open 2023!</p>

<img class="img-fluid float-sm-end profile-pic" src="/media/img/seb.png" width="300">


## About Seb Chan

Seb Chan is the Director & CEO of [ACMI](https://www.acmi.net.au/) in Naarm (Melbourne). 
Appointed to the role in August 2022, Seb was previously a key part of the team which transformed ACMI into a multi-award winning, multiplatform museum. 
Seb initially joined ACMI in the role of Chief Experience Officer in 2015. 
As the senior executive responsible for the Experience & Engagement division of the museum, he guided teams responsible for visitor experience, marketing, brand & communication design, digital products, technology. 
Seb also oversaw the museum's collections, digitisation & digital preservation programs. 
He designed and leads ACMI's CEO Digital Mentoring Program, working with CEOs and directors across the Australian arts and cultural sector. 
He is currently the National President of the Australian Museums and Galleries Association.

Prior to ACMI, Seb led the digital renewal and transformation of the Cooper Hewitt Smithsonian Design Museum in New York and the Powerhouse Museum's pioneering work in open access, mass collaboration and digital experience during the 2000s. 
His work has won awards internationally in the museum, media and design spheres. 
Seb is Adjunct Professor, School of Media and Communications, in the College of Design and Social Context at RMIT, an international advisory board member of Art Science Museum (Singapore) and board member of the National Communications Museum (Melbourne). 
He is an alumnus of the Getty Leadership Institute, Salzburg Global Seminar and UNSW. 
Seb also leads a parallel life in digital art, writing and music.


## Attend Everything Open

Tickets to attend Everything Open 2023 are now on sale!
Make sure you [register today](/attend/tickets/) to hear from Seb and the rest of our great lineup.

While you are getting your ticket, make sure to check out our range of merchandise available for purchase via our [Redbubble shop](https://www.redbubble.com/shop/ap/140087437).
We have a number of different items available for purchase so you can get a memento of attending Everything Open 2023.

If you have questions please contact the Organising Team via email at [contact@everythingopen.au](mailto:contact@everythingopen.au).
