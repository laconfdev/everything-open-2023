---
layout: post
title: "Call for Sessions Extended"
card: calls-open.7e0c78f4.png
---

<p class="lead">The Call for Sessions has been extended until Sunday 22 January 2023.</p>


## Call for Sessions

You asked - we delivered!
We have had many requests to extend call for sessions.
In response to these requests we've extended the deadline to submit session ideas for Everything Open 2023 by a week.

Please submit them by 11:59pm Sunday 22 January 2023 Anywhere on Earth (AoE).
See our [proposals page](/programme/proposals/) for more information and details on how to submit your session proposal.

Sidenote: we are absolutely blown away by the quality of submissions so far and we can't wait to bring this conference to life in March.
Thank you to those who did submit their submissions on time, we really do appreciate it and we are looking at them as we speak.

We are looking forward to announcing our keynote speakers shortly!
It also won't be long until tickets go on sale, so keep an eye out.


## COVID-19 and Everything Open

Linux Australia would like all Everything Open 2023 attendees to have a safe and enjoyable time at the conference.
In light of ongoing COVID-19 concerns, we strongly encourage the wearing of masks and social distancing while at the conference.
Ensuring you are fully vaccinated is another way to minimise the risks associated with COVID-19.
Additionally, we will adhere to public health guidelines and protocols in place at the time.
The conference venue has its own [VenueSafe plan](https://mcec.com.au/venuesafe/visitor) which outlines the steps they take to keep visitors safe.

Without the backing of legislative mandates for mask wearing and vaccination status, Linux Australia has determined that it is not feasible to impose or enforce such mandates at Everything Open 2023.
Doing so would also put extra strain on our volunteer effort especially as we are unable to enforce this on members of the public that will be attending other events in the venue at the same time.

To help minimise the spread of COVID-19, Linux Australia asks that individuals showing COVID-19 symptoms refrain from attending the conference.
As outlined in the conference [Terms and Conditions](/attend/terms-and-conditions/), a full refund will be offered in this instance.

For further information about the precautions being taken by the Everything Open 2023 organising team, please refer to the conference [COVID-19 Statement](/attend/covid-19/) and the [Terms and Conditions](/attend/terms-and-conditions/). If you have remaining questions please contact the Organising Team via email at [contact@everythingopen.au](mailto:contact@everythingopen.au).
