---
layout: post
title: "Ticket Sales Open and Schedule Published"
card: tickets.c5cb5ecc.png
---

<p class="lead">Tickets to Everything Open 2023 are now on sale, and the schedule has been published!</p>


## Schedule

We would like to thank everyone who submitted a proposal to our Call for Sessions.
Our Session Selection Committee has reviewed all of these and selected over 50 presentations to be held during the conference.
It has been encouraging to see the variety of topics proposed by members of the community and we are looking forward to the presentations - there is certainly something for everyone!

Our [schedule](/schedule/) is now available for your perusal.
We will be running Lightning Talks on the Thursday afternoon, with talk submissions open at the event.
Further details on our events will be published closer to the conference.


## Tickets

Tickets to attend Everything Open 2023 are now on sale!
For the first week we also have early bird discounts available, while stocks last.

Prices and inclusions are available on our [tickets](/attend/tickets/) page. Buy your ticket now via your [dashboard](/dashboard/).

Please remember that all attendees need to abide by our [Code of Conduct](/attend/code-of-conduct/).
For information about the precautions being taken by the Everything Open 2023 organising team regarding COVID-19, please refer to the conference [COVID-19 Statement](/attend/covid-19/) and the [Terms and Conditions](/attend/terms-and-conditions/).
If you have questions please contact the Organising Team via email at [contact@everythingopen.au](mailto:contact@everythingopen.au).


## Open Hardware Workshop and Social Event

The Open Hardware team are putting on a special event on Monday 13 March, the day before the conference opens (a public holiday in Victoria, Tasmania, South Australia and Australian Capital Territory).
This event is an all day workshop for the OHMC 2022 hardware, held at the [Melbourne HackerSpace](https://www.hackmelbourne.org/) (CCHS).
The morning session will be based on prepared material about the hardware, the software, how to develop on and use your SwagBadge / Rockling FPGA.
The afternoon session will be open ended, supporting development using your new kit.

Also on that Monday, from late afternoon, an Everything Open social event will be held at the HackerSpace and potentially overflowing on to the nearby Hawthorn Hotel into the evening.
Everyone is welcome to attend this event, regardless of whether you attended the workshop during the day.

Everyone who has a SwagBadge 2021 is encouraged to bring it along to Everything Open, because it is largely compatible with SwagBadge 2022, especially regarding microPython, ESP32 and the Simple-Add-On (SAO) boards.
It will be great to see people putting these to use during the event!

If you are interested in either of these events, we encourage you to fill out the [Open Hardware 2023 event](https://docs.google.com/forms/d/1XxxYti2rxYYmd5JR9wf2oQddNSc-K6iJa4PmApPLfc0/edit) form to help with planning.


## Volunteering

We still have spaces available for volunteers during the conference.
As Everything Open is a community-focused, grassroots conference, it requires the support of volunteers to make the event possible.
Please see the [volunteers](/attend/volunteer/) page for full details and to apply.
