---
layout: post
title: "Keynote: Rebecca Giblin"
card: keynote-rebecca.8bcf1167.png
---

<p class="lead">Professor Rebecca Giblin will be a keynote speaker at Everything Open 2023!</p>

<div class="float-sm-end profile-pic">
  <img class="img-fluid" src="/media/img/rebecca.png" width="450">
  <p>Photo Credit: Ivanna Oksenyuk</p>
</div>

## About Rebecca Giblin

Professor Giblin is an ARC Future Fellow and Professor at Melbourne Law School, and the Director of the [Intellectual Property Research Institute of Australia](http://ipria.org/). 
Her work focuses on a diverse range of topics: the legal and social impacts of library [eLending](http://elendingproject.org/) projects, how fuller protection of creators' rights can help get them paid and simultaneously reclaim lost culture, access to knowledge and culture, technology regulation, and copyright. 
Professor Giblin also leads the ARC-funded [Author's Interest](https://authorsinterest.org) as well as [Untapped: the Australian Literary Heritage Project](https://untapped.org.au/). 
Her new book [Chokepoint Capitalism](https://chokepointcapitalism.com/), with Cory Doctorow, explores how we can recapture creative labour markets from Big Tech and Big Content to get artists paid.


## Attend Everything Open

With the conference only a week away, now is the time to purchase your ticket to attend Everything Open 2023!
Make sure you [register today](/attend/tickets/) to hear from Rebecca and the rest of our great lineup.

We also have a number of different items available for purchase through via our [Redbubble shop](https://www.redbubble.com/shop/ap/140087437).
Now is a great time to get your memento of Everything Open 2023, especially during the last couple of days in their 20% off sale.

If you have questions please contact the Organising Team via email at [contact@everythingopen.au](mailto:contact@everythingopen.au).
