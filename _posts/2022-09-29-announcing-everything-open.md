---
layout: post
title: "Announcing Everything Open"
card: launch.3b728e8c.png
---

<p class="lead">Australasia's grassroots Free and Open Source technologies conference, Everything Open, will be held in Melbourne, Australia from March 14-16 2023.</p>

Linux Australia is proud to announce Everything Open, a new conference that will be running in March 2023.
This three day conference will have presentations on a range of open technologies topics from community members and project leaders.
There will be a key focus on Linux, open source software and open hardware, as well as the communities that surround them.

Following two years of online conferences, we know there is an appetite from many people in our communities to come together in person again.
Everything Open will run in Naarm (Melbourne) across three days, 14-16 March, at the Melbourne Convention and Exhibition Centre.
We realise that some people will need to access the conference from other locations, and we are working hard to maximise access for all.

Everything Open is a grassroots conference with a focus on open technologies, the community that has built up around this movement and the values that it represents.
The presentations will cover a broad range of subject areas including Linux, open source software, open hardware, open data, open government, and open GLAM (galleries, libraries, archives and museums), to name a few.
As we have come to expect, there will be technical deep-dives into specific topics from project contributors, as well as tutorials on building hardware or using a piece of software, not to mention talks covering the inner workings of our communities.

## Call for Sessions

Our Call for Sessions will open in a couple of weeks.
We encourage you to start thinking about talks to present at the conference, ready to submit a proposal once the dashboard opens.

## Sponsorship opportunities

We have a wide range of sponsorship opportunities available.
In addition to sponsoring the conference overall, there are a number of opportunities to contribute towards specific parts of the event.
If you or your organisation is interested in sponsoring Everything Open, please [get in touch](../../sponsors/prospectus/).

## Stay in the know

Subscribe to our announcement mailing list and follow our social channels to be the first to know about ticket sales, speaker lineups and conference highlights.

* [Mailing list](https://lists.linux.org.au/mailman/listinfo/eo-announce)
* [Twitter](https://twitter.com/_everythingopen)
* [LinkedIn](https://www.linkedin.com/showcase/everythingopen/)
* [Conference website](https://2023.everythingopen.au/)
