---
layout: post
title: "Keynote: Hugh Blemings"
card: keynote-hugh.26a4168d.png
---

<p class="lead">Everything Open would like to announce our third keynote speaker: Linux Australia's very own Hugh Blemings!</p>

<img class="img-fluid float-sm-end profile-pic" src="/media/img/hugh.jpg" width="300">


## About Hugh Blemings

Hugh has had a long standing association with Free and Open Source Software, Open Hardware and, in particular, Linux and POWER/PowerPC. His career has spanned everything from Linux kernel development to engineering team management, electronics design to technical program management at the likes of IBM, Canonical, OpenPOWER Foundation and AWS.

As a participant in the open technical commons, he has served in a voluntary capacity on the Council of Linux Australia in various roles including President, and is a former member of the Linux Foundation's Technical Advisory Board. As Executive Director of the OpenPOWER Foundation he guided the opening of the POWER Instruction Set Architecture (ISA), a move that ensured choice in the open high performance microprocessor space.

Of late Hugh returned to his professional passion - people management - as an Engineering Director leading Grafana Labs' APAC Databases team, a great bunch of folk working on some equally great software. Outside the FOSS/technical world Hugh is involved in various musical endeavours in and around his home in regional Victoria. This latter recently involved playing the role of a somewhat forgetful theatre cat...


## Attend Everything Open

With the conference rapidly approaching, now is the time to purchase your ticket to attend Everything Open 2023!
Make sure you [register today](/attend/tickets/) to hear from Hugh and the rest of our great lineup.

We also have a number of different items available for purchase through via our [Redbubble shop](https://www.redbubble.com/shop/ap/140087437).
Now is a great time to get your memento of Everything Open 2023, especially during the last couple of days in their 20% off sale.

If you have questions please contact the Organising Team via email at [contact@everythingopen.au](mailto:contact@everythingopen.au).
