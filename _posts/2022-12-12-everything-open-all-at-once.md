---
layout: post
title: "Everything Open, All At Once!"
card: calls-open.7e0c78f4.png
---

<p class="lead">The Everything Open 2023 organising team is excited to announce:
<ul>
  <li>Call for Sessions</li>
  <li>Call for Volunteers</li>
  <li>Financial Assistance applications</li>
</ul>
ARE NOW OPEN!!!
</p>

<hr>

Wominjeka Everyone,

Everything Open will run over three days - March 14-16, 2023 - in Naarm (Melbourne) at the Melbourne Convention and Exhibition Centre.
We are excited to be opening our Call for Sessions to get another wide range of talks and tutorials in our schedule.
We also need a team of volunteers to help us achieve success with this conference.

## Call for Sessions

We invite you to submit a session proposal on a topic you are familiar with via our [proposals portal](/programme/proposals/).
The Call for Sessions will remain open until 11:59 pm on Sunday 15 January 2023 anywhere on earth (AoE).

There will be multiple streams catering for a wide range of interest areas across the many facets of open technology, including Linux, open source software, open hardware, standards, formats and documentation, and our communities.
In keeping with the conference's aim to be inclusive to all community members, presentations can be aimed at any level, ranging from technical deep-dives through to beginner and intermediate level presentations for those who are newer to the subject.
Where possible, talks on a related subject will be arranged sequentially in the schedule.

There will be two types of sessions at Everything Open: talks and tutorials.
Talks will nominally be 45 minutes long on a single topic presented in lecture format.
We will also have a few short talk slots (25 minutes) available, which are perfect for people new to presenting at a conference.
Tutorials are interactive and hands-on in nature, presented in classroom format.
Each accepted session will receive one Professional level ticket to attend the conference.

The Session Selection Committee is looking forward to reading your submissions.
We would also like to thank them for coming together and volunteering their time to help put this conference together.


## Call for Volunteers
We're calling on people to come forward and lend a hand again.
If you've done it all before and just want to get on with signing up, all the details can be found at the [volunteers page](/attend/volunteer).

If you're new to it all and want to help out, keep reading!

We're all volunteers ourselves and we need more – we can't run the conference by ourselves, particularly during the week of the conference itself.
We need help with:

* Checking attendees in when they arrive
* Operating AV equipment such as audio gear and cameras
* Directing people around our venue
* Ensuring talks and tutorials run to schedule
* Setting up and packing up the conference

Anyone who has volunteered for a Linux Australia event before will tell you it's a very busy time, but also very worthwhile.
It's satisfying to know that you've helped everyone at the conference to get the most out of it.
It's very rewarding knowing that you've made a positive difference to someone's day.

You don't just get to meet the delegates and speakers, you also get to know many of them while helping them out.
You are presented with a unique opportunity to get the behind the scenes and close to the action.
You'll get to forge new relationships with amazing, interesting and wonderful people (just like you), whom you might not have otherwise had the good fortune to meet in any other way.

In return for your help we'll provide you with:
* Food - morning tea, lunch and afternoon tea
* A clean T shirt everyday
* If you want one, a letter of reference at the end of the conference

Depending on the number of volunteers we get and workload (many hands make light work), we'll do our best to allocate you to roles so that you can attend talks that interest you.

For more information, please check out our [volunteers page](/attend/volunteer), where we have full details of what we need assistance with.
We review and approve applications regularly.

## Financial Assistance

We have budget set aside for providing financial assistance to Everything Open attendees who might otherwise find it difficult to attend.
This program is a key part of our outreach and inclusion efforts for Everything Open.

Anyone can apply for financial assistance for Everything Open 2023.
The assistance can cover things such as a ticket to the conference, travel, accommodation, or other costs that you would incur to attend.
We will be processing applications on a regular basis until funds are exhausted, so we encourage you to apply early for a greater chance of being accepted.

Full details about the program and the application form can be found in our [Financial Assistance guide](/attend/assistance/).

If you have any other questions you can contact us via email at [contact@everythingopen.au](mailto:contact@everythingopen.au).





