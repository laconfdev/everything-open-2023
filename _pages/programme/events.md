---
layout: page
title: Events
permalink: /programme/events/
sponsors: true
---

During Everything Open we will be running some events in addition to the talks and tutorials.
These are both official and unoffical events for our attendees.

If you have any last minute questions please head to the registration desk, or email [contact@everythingopen.au](mailto:contact@everythingopen.au).

---

<a id="open-hardware"></a>
## Open Hardware Workshop

Monday 13 March, 9am - 4pm

_Requires registration via the Open Hardware [Registration Form](https://docs.google.com/forms/d/1XxxYti2rxYYmd5JR9wf2oQddNSc-K6iJa4PmApPLfc0/edit)_

#### Overview

An all day workshop for people with the OHMC 2022 hardware.
The morning session will be based on prepared material about the hardware, the software, how to develop on and use your SwagBadge / Rockling FPGA.
The afternoon session will be open ended, supporting development using your new kit.

Following the workshop there will be an unofficial social event held at the hackerspace or a venue nearby.
Anyone is welcome to attend.

For full details, visit the Open Hardware Miniconf [2023 Overview](http://www.openhardwareconf.org/wiki/OHMC2023).

#### How do I get there?

The workshop will be held at the [Connected Community Hackerspace](https://www.hackmelbourne.org/).
This is located at 5 Kent Lane, Hawthorn.

To get there, take a Metro train from Southern Cross or Flinders Street to Glenferrie station - Alamein, Belgrave or Lilydale lines.

<div class="map-responsive">
    <iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=145.0373759865761%2C-37.82431093712212%2C145.0402727723122%2C-37.822435896396975&amp;layer=mapnik&amp;marker=-37.823374450066076%2C145.03882550000003" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat=-37.82337&amp;mlon=145.03883#map=19/-37.82337/145.03883&amp;layers=N">View Larger Map</a></small>
</div>

---

<a id="penguin-dinner"></a>
## Penguin Dinner

Wednesday 15 March, 6:00pm

_For Penguin Dinner ticket holders only._

#### Overview

* Penguin Dinner starts at 6:00pm at [Fortress Melbourne](https://www.fortress.games/)
* You must have a non-zero number next to the Penguin Dinner icon on your badge to attend
* Activities start at 6pm and will conclude at 9pm
* You are welcome to head to the venue earlier or stay later to play board games, consoles, etc

#### How do I get there?

Make your way to Fortress Melbourne at **23 Caledonian Lane, Melbourne VIC 3000**.
Please enter via the **Caledonian Lane entrance**, NOT via the Emporium.

You will have your badge checked on arrival before entering for the dinner. No badge, no entry.

<div class="map-responsive">
    <iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=144.96289640665054%2C-37.81295826683023%2C144.96579319238663%2C-37.81108293771237&amp;layer=mapnik&amp;marker=-37.81202166773332%2C144.96434479951859" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat=-37.81202&amp;mlon=144.96434#map=19/-37.81202/144.96434&amp;layers=N">View Larger Map</a></small>
</div>
