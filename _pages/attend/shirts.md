---
layout: page
title: Shirts and Swag
permalink: /attend/shirts/
sponsors: true
---

We are pleased to offer a range of shirts and other items through Redbubble so you can get a memento of attending Everything Open 2023.

In past years we have provided shirts as part of the registration, however we often end up with a lot of unused shirts that go to waste.
By moving everything to an online store we can offer a wider range of items (shirts, laptop sleeves, bags, coffee mugs, etc) while minimising waste.

We look forward to seeing you with your Everything Open 2023 merchandise at the conference and on social media!

<a href="https://www.redbubble.com/shop/ap/140087437" class="btn btn-outline-primary" role="button">Buy Shirts and Swag</a>
