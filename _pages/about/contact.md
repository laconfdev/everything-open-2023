---
layout: page
title: Contact
permalink: /about/contact/
sponsors: true
---

## Contact the Organising Team

Is there something you would like to know that isn't on this site? Please contact us via email.

<i class="bi-envelope-fill"></i> Email: [contact@everythingopen.au](mailto:contact@everythingopen.au)

## Safety



## Stay up to date

We have several channels that we use to post announcements in the lead up to, and during, the conference.

* <i class="bi-mastodon"></i> Mastodon: [@EverythingOpen@fosstodon.org](https://fosstodon.org/@EverythingOpen), hashtag #eo2023
* <i class="bi-twitter"></i> Twitter: [@_everythingopen](https://twitter.com/_everythingopen), hashtag #eo2023
* <i class="bi-linkedin"></i> LinkedIn: [Everything Open](https://www.linkedin.com/showcase/everythingopen/)
* <i class="bi-inboxes-fill"></i> Announce mailing list: [Everything Open Announce](https://lists.linux.org.au/mailman/listinfo/eo-announce)

## Contact Linux Australia

Linux Australia is the auspice for Everything Open.
Check out their website for more details and contact information [linux.org.au](https://linux.org.au)
