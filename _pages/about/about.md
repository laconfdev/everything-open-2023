---
layout: page
title: About
permalink: /about/
sponsors: true
---

## Important Information

Everything Open 2023 will be held at the Melbourne Convention and Exhibition Centre from March 14-16 2023.
The conference will be run in the Australian Eastern Daylight Time (AEDT - UTC+11) timezone.

## About Everything Open

Everything Open is running for the first time in 2023.
Linux Australia has decided to run this event to provide a space for a cross-section of the open technologies communities to come together in person.
The conference draws upon the experience of the many events that have been run by Linux Australia and its subcommittees, starting with CALU (Conference of Australian Linux Users) in 1999, linux.conf.au over the past twenty years, and the Open Source Developers Conference (OSDC).

Everything Open is a grassroots conference with a focus on open technologies, the community that has built up around this movement and the values that it represents.
The presentations cover a broad range of subject areas, including Linux, open source software, open hardware, open data, open government, open GLAM (galleries, libraries, archives and museums), to name a few.
There are technical deep-dives into specific topics from project contributors, as well as tutorials on building hardware or using a piece of software, not to mention talks covering the inner workings of our communities.

At the core of Everything Open is the community.
The conference is entirely organised by volunteers who have a passion for bringing together the open technologies communities to share their collective experience.
Everything Open is a not for profit event that aims to provide attendees with a world-class conference at a down to earth rate.

## Linux Australia

[Linux Australia](https://linux.org.au) represents approximately 5000 Australian users and developers of Free Software and Open Technologies, and facilitates internationally-renowned events.
Linux Australia provide the financial, technical and insurance infrastructure for Everything Open.

## Our Team

Everything Open 2023 is organised by a core team of volunteers who contribute many hours of their time to run this event.
In addition to this core team there are many other contributors who put in significant effort to make this a successful event.

 * Clinton Roy
 * Joel Addison
 * Jonathan Woithe
 * Neill Cox
 * Sae Ra Germaine
 * Steve Walsh
