---
layout: page
title: Location
permalink: /about/location/
sponsors: true
---

## The Location

Naarm (Melbourne) will be our first destination for Everything Open and Linux Australia is returning for for the first time since 2008.
Melbourne is Australia's cultural capital playing host to cafes, fantastic bars, restaurants, museums, galleries surrounded by large parks and gardens.
And you know we have the best coffee!

Take the [virtual tour](https://www.melbournecb.com.au/welcome-to-melbourne/) to look around Naarm and all that we have to offer in our wonderful city.

## The Venue

The [Melbourne Convention and Exhibition Centre](https://mcec.com.au/) (MCEC), located on the lands of the the Boon Wurrung and Wurundjeri Woi Wurrung people of the Kulin nation along the banks of the Birrarung (Yarra River) in South Wharf, will be be our meeting space during our conference.
There are many options for accommodation and plenty of public transport that is right at the doorstep at MCEC.
The venue is the perfect place for us to kick off this inaugural event.

MCEC offers large open spaces to welcome conversation, share knowledge, and catch up with old friends.
We will be using the Clarendon Rooms for the event, which are located at the Crown Casino end of the main exhibition building.
The [Clarendon Auditorium](https://mcec.com.au/plan/rooms-and-spaces/clarendon-auditorium-and-foyer) will host our keynote presentations and one of the breakout session tracks, while the sourrounding Clarendon Rooms will host our other breakout sessions.

The registration desk will be located in the Clarendon Auditorium Foyer.
Please enter the MCEC via the doors from Clarendon St, then use the stairs to the left of the information desk to make your way to the foyer.
The registration desk is located at the top of the stairs.

<div class="map-responsive">
    <iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=144.94779825210574%2C-37.82825154540846%2C144.96392369270328%2C-37.82070064677696&amp;layer=mapnik&amp;marker=-37.824480429981186%2C144.9558663368225" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat=-37.82448&amp;mlon=144.95587#map=17/-37.82448/144.95586">View Larger Map</a></small>
</div>
